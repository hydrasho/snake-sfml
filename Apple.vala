using sf;

public class Apple : Cube {
	public Apple () {
		base (Color(255, 0, 0));
		randomize ();
	}

	public void randomize () {
		var x = Random.int_range (1, 24);
		var y = Random.int_range (1, 24);
		this.position = {x * 25, y * 25};
	}
}
