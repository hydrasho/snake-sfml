using sf;

public class Cube : RectangleShape{
	public Cube (Color color = Color.White, Vector2f position = {0, 0}) {
		this.setFillColor(color);
		this.setSize(Vector2f(25, 25));
		this.setPosition(position);
		this.setOutlineThickness(-2.0f);
		this.setOutlineColor(Color(30, 30, 30));
	}
}
