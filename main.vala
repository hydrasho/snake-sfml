using sf;

void main() {
	var window = new RenderWindow(VideoMode(650, 650), "Snake") {
		fps=120
	};
	var texture = new Texture.fromFile("img/background.png");
	var background = new Sprite() {
		texture = texture
	};
	var scene = new Scene();

	var cube = new Cube(Color.from_RGB(21, 23, 25));

	while (window.isOpen()) {
		Event event;
		while (window.pollEvent(out event)) {
			if (event.type == EventType.Closed)
				return;
			scene.event(event, window);
		}

		window.clear();

		window.drawSprite(background);
		cube.position = {0, 0};
		cube.setOutlineThickness(0f);
		for (var i = 0; i < 26; ++i)
		{
			cube.position = {0, i * 25};
			cube.draw(window);
			cube.position = {i * 25, 0};
			cube.draw(window);
			cube.position = {625, i * 25};
			cube.draw(window);
			cube.position = {i * 25, 625};
			cube.draw(window);
		}
		scene.draw (window);

		window.display();
	}
}
