using sf;

public class Menu {
	public Menu () {
		t_title = new Texture.fromFile("img/snake.png");
		t_wait  = new Texture.fromFile("img/appuyez.png");
		s_title = new Sprite() {
			texture = t_title,
			position = {0, 100}
		};
		s_wait  = new Sprite() {
			texture = t_wait,
			position = {0, 400}
		};
		timer = new Timer();
	}

	public void draw (RenderWindow window) {
		window.drawSprite (s_title);
		if (timer.elapsed () >= 0.60) {
			anime = !anime;
			timer.reset ();
		}
		if (anime)
			window.drawSprite (s_wait);
	}

	public void event (Event event, RenderWindow window) {
		if (event.key.code == KeyCode.Space || event.key.code == KeyCode.Enter)
			activity = GAME;
	}

	private Timer timer;
	private bool anime;
	private Texture t_title;
	private Sprite s_title;
	private Sprite s_wait;
	private Texture t_wait;
}
